<%def name="result_cell(result, link, letter, test_name, build, tooltip)">
  <td class="${result}">
    <div class="tooltip">
      <a href="${link}">${letter}</a>
      % if tooltip:
	<span>
	  <b>${test_name}<br/>${build}</b><br/>
	  <pre><code>${tooltip}</code></pre>
	</span>
      % endif
    </div>
  </td>
</%def>
