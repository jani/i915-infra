#!/usr/bin/env python3
#
# piglit results.json visualizer over history and hosts
#
# Originally by Tomi Sarvela for Intel OTC lab
#
import os
import argparse

from mako.lookup import TemplateLookup
from mako import exceptions

from visutils import *
from common import htmlname

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description="Visualize set of piglit json results")
    ap.add_argument('-o','--output', help="Output html prefix", type=str, default='')
    ap.add_argument('--heatmap', help="Create heatmap", default=False, action='store_true')
    ap.add_argument('--combine', type=str, default=None,
        help="Deprecated| Combine results for similar hosts")
    ap.add_argument('--testsort', default=False, action='store_true',
        help="Sort tests in alphabetical order")
    ap.add_argument('-d', '--depth', type=int, default=0,
        help="Relative dir depth of html files, added to links")
    ap.add_argument('--buildorder', type=str, default=None,
        help="Filename for build order information")
    ap.add_argument('files', help="Files to be visualized", metavar='files', nargs='+', type=str)
    ap.add_argument('-a','--hostalias-file', type=str, default=None,
        help="Combine results for similiar hosts based on RegEx file")

    args = ap.parse_args()

    relpath = "../"*args.depth

    if args.buildorder: readbuildorder(args.buildorder)

    if args.hostalias_file:
        try:
            read_hostalias_file(args.hostalias_file)
        except Exception as e:
            print("Err:",e)
            exit(1)
    else:
        try: 
            read_hostalias_file(os.path.join(os.path.dirname(os.path.realpath(__file__)), "hostaliases.rx"))
        except: pass

    try: jsons = readfiles(args.files, combine=args.combine) # arranged by [(build,host)]
    except (KeyboardInterrupt) as e:
        print("ERROR: Keyboard interrupt while reading files")
        exit(1)

    tests = parsetests(jsons, sort=args.testsort)
    # keep the order same on these two arrays
    hostcols = [host for _,host in sorted(jsons, key=grouphosts)]
    buildcols = [build for build,_ in sorted(jsons, key=grouphosts)]

    rootdir = os.path.dirname(os.path.abspath(__file__))
    mako = TemplateLookup([os.path.join(rootdir, "templates"), rootdir])

    try: template = mako.get_template('history-host.mako')
    except:
        print(exceptions.text_error_template().render())
        exit(1)

    with open(args.output+"hosts.json", 'w') as f:
        j={'hosts': [host for host in sorted(set(hostcols))]}
        f.write(json.dumps(j))

    for host in set(hostcols):
        builds = [b for b,h in sorted(jsons, key=grouphosts) if h==host]
        # Show only the tests that have changed result between builds
        hosttests=[]
        for test in tests:
            res=None
            for build in builds:
                try:
                    newres=jsons[(build,host)]['tests'][test]['result']
                except: continue
                if newres in ['notrun', 'none']: continue
                if not res: res = newres
                if res != newres:
                    hosttests.append(test)
                    break
            else:
                if res and res not in ['pass', 'skip']:
                    hosttests.append(test)

        with open(args.output+host+".html", 'w') as f:
            try:
                page = template.render(path=relpath, builds=builds, tests=hosttests, host=host, jsons=jsons)
                f.write(page)
            except:
                print(exceptions.text_error_template().render())
                exit(1)

    try: template = mako.get_template('history-test.mako')
    except:
        print(exceptions.text_error_template().render())
        exit(1)

    hosts = []
    [hosts.append(host) for host in hostcols if host not in hosts]

    for test in set(tests):
        with open(args.output+htmlname(test), "w") as f:
            try:
                page = template.render(path=relpath, builds=builds, test=test, hosts=hosts, jsons=jsons)
                f.write(page)
            except:
                print(exceptions.text_error_template().render())
                exit(1)

    if args.heatmap:
        try: template = mako.get_template('heatmap.mako')
        except:
            print(exceptions.text_error_template().render())
            exit(1)

        totals={} # (host,test) = total
        errors={} # (host,test) = errors
        builds=set()
        for build,host in jsons:
            if (build,host) not in jsons or jsons[(build,host)] is None: continue
            if 'tests' not in jsons[(build,host)]: continue
            for testname in jsons[(build,host)]['tests'].keys():
                test=jsons[(build,host)]['tests'][testname]
                if 'result' not in test: continue
                res=test["result"]
                # don't add neither counter if test not run
                if res in ['none', 'notrun']: continue
                if (host,testname) not in totals:
                    totals[(host,testname)]=0
                    errors[(host,testname)]=0
                totals[(host,testname)]+=1
                # don't add errors if test is successful
                if res in ['pass', 'skip']: continue
                errors[(host,testname)]+=1

        # Filter out the heatmap lines without errors
        for test in tests[:]:
            if not any((errors.get((host,test)) for host in hosts)):
                tests.remove(test)

        # Filter out the non-test
        try: tests.remove('igt@runner@aborted')
        except: pass

        # Write heatmap out
        with open(args.output+"heatmap.html", 'w') as f:
            try:
                page = template.render(path=relpath, tests=tests, hosts=hosts, errors=errors, totals=totals, title="Heatmap")
                f.write(page)
            except:
                print(exceptions.text_error_template().render())
                exit(1)


    exit(0)
